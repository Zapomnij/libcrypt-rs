use crate::*;
fn default(salt: &str, data: &str) -> Result<String, ()> {
	let mut crypt = Crypt::new();

	crypt.set_salt(salt.to_string()).expect("Setting custom salt failed");
	crypt.encrypt(data.to_string()).expect("Encryption failed");

	return Ok(crypt.encrypted);
}

#[test]
fn sha256crypt() -> Result<(), ()> {
	assert!(default("$5$zEWKjlvKcB.tTrrc", "1234").unwrap().eq("$5$zEWKjlvKcB.tTrrc$dwkLNSw87J0HDjz0dbyzC1t6mJQMsYmLuOoDvF2kRuC"));

	Ok(())
}

#[test]
fn sha512crypt() -> Result<(), ()> {
	assert!(default("$6$3yXQfeBr/PuHVzmk", "1234")
		.unwrap()
		.eq("$6$3yXQfeBr/PuHVzmk$dDOm6sZ8GEwvV2FjN5PgVyHSexSJVvZByBxHSoQzCBBh/bS9px.zZcb0obNKdX/YP9wfW7HSQxy/EMRcnSjiV0")
	);

	Ok(())
}

#[test]
fn md5crypt() -> Result<(), ()> {
	assert!(default("$1$M2QAk.DR", "1234").unwrap().eq("$1$M2QAk.DR$1u.2sl8BqSPNc0ktj/dmH/"));

	Ok(())
}

#[test]
fn yescrypt() -> Result<(), ()> {
	assert!(default("$y$j9T$vQHMOPjUa5lQwhlvhz5T91", "1234").unwrap().eq("$y$j9T$vQHMOPjUa5lQwhlvhz5T91$M8fVEPsjT5PUYyDJvtFejWzqDcwkoB5Mjp7PbBCjrQ0"));

	Ok(())
}