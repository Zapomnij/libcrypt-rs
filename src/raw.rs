//! Raw bindings to libcrypt. unsafe rust required

use std::os::raw::*;

/// Isn't created for libcrypt interaction. usized.
pub const CRYPT_OUTPUT_SIZE: usize = 384;
/// Isn't created for libcrypt interaction. usized.
pub const CRYPT_MAX_PASSPHRASE: usize = 512;

pub const CRYPT_SALT_OK: c_int = 0;
pub const CRYPT_SALT_INVALID: c_int = 1;
pub const CRYPT_SALT_METHOD_DISABLED: c_int = 2;
pub const CRYPT_SALT_METHOD_LEGACY: c_int = 3;
pub const CRYPT_SALT_TOO_CHEAP: c_int = 4;

extern "C" {
	pub fn crypt(phrase: *const c_char, setting: *const c_char) -> *const c_char;

	pub fn crypt_gensalt(phrase: *const c_char, count: c_ulong, rbytes: *const c_char, nrbytes: c_int) -> *const c_char;

	pub fn crypt_preferred_method() -> *const c_char;

	pub fn crypt_checksalt(setting: *const c_char) -> c_int;
}
