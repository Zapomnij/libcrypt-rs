//! Encryption methods.
pub enum Encryptions {
	/// MD5 encryption method.
	Md5,
	/// sha256 encryption method.
	Sha256,
	/// sha512 encryption method.
	Sha512,
	/// yescrypt encryption method.
	Yescrypt,
}

impl Encryptions {
	/// Returns salt prefix for selected encryption method.
	/// ```
	/// use libcrypt_rs::Encryptions;
	/// 
	/// let encryption = Encryptions::Md5;
	/// println!("Salt prefix for MD5 encryption: '{}'", encryption.decode());
	/// ```
	pub fn decode(&self) -> String {
		match self {
			Self::Md5 => return "$1$".to_string(),
			Self::Sha256 => return "$5$".to_string(),
			Self::Sha512 => return "$6$".to_string(),
			Self::Yescrypt => return "$y$".to_string()
		}
	}
}