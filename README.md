# libcrypt-rs - rust binding for POSIX crypt library (libcrypt)

## How to use it
Add to the `dependencies` section in your `Cargo.toml`:
```toml
libcrypt-rs = "0.1.2"
```

## Documentation
`cargo` has cool feature for generating documentation:
- Run
```sh
cargo doc
```
- Open in web browser `target/doc/libcrypt_rs/index.html`

## Example
```rs
use libcrypt_rs::{Crypt, Encryptions};

fn main() {
	let mut crypt_engine = Crypt::new();

	crypt_engine.gen_salt(Encryptions::Sha256).expect("Salt generating failed");
	crypt_engine.encrypt("1234".to_string()).expect("Encryption failed");

	println!("Encrypted data: {}", crypt_engine.encrypted);
}
```

## Notices
- Do not encrypt multiple data at the same time, otherwise can cause segmentation fault.